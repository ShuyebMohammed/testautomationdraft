package sitepassSelfRegistration;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import static org.testng.internal.Utils.split;

public class BaseTest{

    public static WebDriver driver;
    public Properties testproperties;
    private String BrowserType;
    private String Propertiesfilename = "config.properties";



    public void InitialiseBrowser() throws Exception {

        testproperties = new Properties();
        InputStream inputstream = getClass().getClassLoader().getResourceAsStream(Propertiesfilename);
        if (inputstream != null) try {
            testproperties.load(inputstream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        BrowserType = testproperties.getProperty("Browser");

            if (BrowserType.contentEquals("Chrome")) {
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.addArguments("--start-maximized");
                System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\src\\test\\resources\\Drivers\\chromedriver.exe");
                driver = new ChromeDriver(chromeOptions);
            } else if (BrowserType.contentEquals("Firefox")) {
                System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "\\src\\test\\resources\\Drivers\\geckodriver.exe");
                driver = new FirefoxDriver();
                driver.manage().window().maximize();
            } else if (BrowserType.contentEquals("IE")) {
                System.setProperty("webdriver.ie.driver", System.getProperty("user.dir") + "\\src\\test\\resources\\Drivers\\IEDriverServer.exe");
                driver = new InternetExplorerDriver();
                driver.manage().window().maximize();
            }else if (BrowserType.contentEquals("Edge")){
                System.setProperty("webdriver.edge.driver", System.getProperty("user.dir")+ "");
                driver = new EdgeDriver();
                driver.manage().window().maximize();
            } else if (BrowserType.contentEquals("Phantom")){
                System.setProperty("phantomjs.binary.path", System.getProperty("user.dir") + "\\src\\test\\resources\\Drivers\\phantomjs.exe");
                WebDriver driver = new PhantomJSDriver();
            }

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }

// Commonly used functions

    public void accessPage(String pageUrl, String expPageTitle) {
        if (driver == null) {
            //LOG.error("getDriver Failed to obtain driver, verify Initialise function");
            throw new NullPointerException();
        }
        driver.get(pageUrl);
        try{
            Assert.assertEquals(expPageTitle, driver.getTitle());
            System.out.println("Navigated to correct webpage");
        }
        catch(Throwable pageNavigationError){
            System.out.println(driver.getTitle());
            System.out.println("Didn't navigate to correct webpage");
        }
    }

     public void clickButton(WebElement webElement) {
        try{
            boolean elementDisplayed = webElement.isDisplayed();
            boolean elementEnabled = webElement.isEnabled();
            if (elementDisplayed == true & elementEnabled == true) {
                //Actions actions = new Actions(driver);
                //actions.moveToElement(webElement).click().perform();

                webElement.click();
            }
        }catch (NoSuchElementException e){
            e.printStackTrace();
        }
    }

    public void inputValue(WebElement webElement, String strValue) {
        try{
            boolean elementDisplayed = webElement.isDisplayed();
            boolean elementEnabled = webElement.isEnabled();
            if (elementDisplayed == true & elementEnabled == true) {
                webElement.clear();
                webElement.sendKeys(strValue);
            }
        }catch (NoSuchElementException e){
            e.printStackTrace();
        }
    }

    public void selectByValue(WebElement webElement, String strValue) {
        try{
            boolean elementDisplayed = webElement.isDisplayed();
            boolean elementEnabled = webElement.isEnabled();
            if (elementDisplayed == true & elementEnabled == true) {
                Select dropDown = new Select(webElement);
                dropDown.selectByValue(strValue);
            }
        }catch (NoSuchElementException e){
            e.printStackTrace();

            }
    }

    public void selectByText(WebElement webElement, String strValue) {
        try{
            boolean elementDisplayed = webElement.isDisplayed();
            boolean elementEnabled = webElement.isEnabled();
            if (elementDisplayed == true & elementEnabled == true) {
                Select dropDown = new Select(webElement);
                dropDown.selectByVisibleText(strValue);
            }
        }catch (NoSuchElementException e){
            e.printStackTrace();

        }
    }


    public void setDateInCalendar(WebElement webElement1, WebElement webElement2, WebElement webElement3, String date) {

        String[] dateString = new String[3];
        dateString = split(date, "/");
        String day = dateString[0]; //31
        if (Integer.parseInt(day)<10){
            day = day.replace("0", "");
         }
        String month = dateString[1]; //03
        String year = dateString[2]; //1980

        switch (month) {
            case "01":
                month = "January";
                break;
            case "02":
                month = "February";
                break;
            case "03":
                month = "March";
                break;
            case "04":
                month = "April";
                break;
            case "05":
                month = "May";
                break;
            case "06":
                month = "June";
                break;
            case "07":
                month = "July";
                break;
            case "08":
                month = "August";
                break;
            case "09":
                month = "September";
                break;
            case "10":
                month = "October";
                break;
            case "11":
                month = "November";
                break;
            case "12":
                month = "December";
                break;
        }

        try {
            boolean elementDisplayed = webElement3.isDisplayed();
            boolean elementEnabled = webElement3.isEnabled();
            if (elementDisplayed == true & elementEnabled == true) {
                Select dropDown = new Select(webElement3);
                dropDown.selectByValue(year);
            }
        } catch (NoSuchElementException e) {
            e.printStackTrace();

        }

        try {
            boolean elementDisplayed = webElement2.isDisplayed();
            boolean elementEnabled = webElement2.isEnabled();
            if (elementDisplayed == true & elementEnabled == true) {
                Select dropDown = new Select(webElement2);
                dropDown.selectByVisibleText(month);
            }
        } catch (NoSuchElementException e) {
            e.printStackTrace();

        }

        try {
            boolean elementDisplayed = webElement1.isDisplayed();
            boolean elementEnabled = webElement1.isEnabled();
            if (elementDisplayed == true & elementEnabled == true) {
                WebElement datePicker = webElement1;
                List<WebElement> dates = datePicker.findElements(By.tagName("td"));
                for(WebElement newDate : dates){
                     String calDate = newDate.getText();
                     if (calDate.contentEquals(day)){
                         newDate.click();
                         break;
                     }
                }
            }
        } catch (NoSuchElementException e) {
            e.printStackTrace();

        }

    }

    public boolean waitForElement(WebElement webElement, int waitTime){
        try{
        WebDriverWait wait = new WebDriverWait(driver, waitTime);
        wait.until(ExpectedConditions.visibilityOf(webElement));
        return true;
        }catch (Exception e){
            return false;
        }
    }

}


