package sitepassSelfRegistration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static org.testng.internal.Utils.split;

public class EmpRegistrationPage {

    private WebDriver driver;

    @FindBy(id= "username")
    public WebElement userName;

    @FindBy(id="password")
    public WebElement passWord;

    @FindBy(id="confirmPassword")
    public WebElement confirmPassword;

    @FindBy(id="timezone")
    public WebElement timeZone;

    @FindBy(id="contractor")
    public WebElement businessName;

    @FindBy(id= "name")
    public WebElement firstName;

    @FindBy(id= "surname")
    public WebElement lastName;

    @FindBy(id= "dateOfBirth")
    public WebElement dateOfBirth;

    @FindBy(xpath = "//form[@id='new_employee_form']/fieldset[3]/div[3]/input[2]")
    public WebElement calendarButton;

    @FindBy (xpath = "//table[@id='cal']/tbody/tr/td[2]/select")
    //@FindBy (name = 'month')
    public WebElement selectMonth;

    @FindBy (xpath = "//table[@id='cal']/tbody/tr/td[2]/select[2]")
    //@FindBy (name = 'year')
    public WebElement selectYear;

    @FindBy (xpath = "//tbody[@id='_JQUERY_CALENDAR_WIDGET_dates']")
    public WebElement datesTable;

    @FindBy(id= "reference")
    public WebElement employeeID;

    @FindBy(id= "email")
    public WebElement eMail;

    @FindBy(id= "phone")
    public WebElement phoneNumber;

    @FindBy(id= "mobile")
    public WebElement mobileNumber;

    @FindBy(id= "fax")
    public WebElement faxNumber;

    @FindBy(id= "country")
    public WebElement country;

    @FindBy(id= "address1")
    public WebElement address;

    @FindBy(id= "suburb")
    public WebElement suburb;

    @FindBy(id= "state")
    public WebElement state;

    @FindBy(id= "postcode")
    public WebElement postCode;

    @FindBy(id= "employeeGroup")
    public WebElement jobType;

    @FindBy(xpath = "//div[@id='employeeCategoryCont']/button")
    public WebElement servicedAreas;

    //@FindBy(id= "ui-multiselect-employeeCategory-option-0")
    @FindBy(xpath = "//input[contains(@id, 'ui-multiselect-employeeCategory')][@title = 'Site']")
    public WebElement servicedAreaOptions;

    @FindBy(id= "addEmployeeCategorey")
    public WebElement addServicedArea;

    @FindBy(id= "clientGroups")
    public WebElement relationShip;

    @FindBy(id= "addClienRelation")
    public WebElement addRelationShip;

    @FindBy(name="accountform")
    public WebElement nextButton;

    @FindBy(id = "invoice")
    public WebElement authNumber;

    @FindBy(id = "activate")
    public WebElement activateButton;

    public EmpRegistrationPage(){
        this.driver = BaseTest.driver;
        PageFactory.initElements(driver, this);
    }

    public String captureAuthCode(){
        String currentURL = driver.getCurrentUrl();
        String[] authCodeURL = new String[2];
        authCodeURL = split(currentURL, "=");
        String authNumber = authCodeURL[1];
        return authNumber;
    }
}

//Specific functions exclusive to this page




