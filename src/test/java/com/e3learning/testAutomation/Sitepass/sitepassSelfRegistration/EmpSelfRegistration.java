package com.e3learning.testAutomation.Sitepass.sitepassSelfRegistration;

//import com.e3learning.testAutomation.empSitepass.pages.StartUpPage;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import sitepassSelfRegistration.BaseTest;
import sitepassSelfRegistration.EmpRegistrationPage;


public class EmpSelfRegistration extends BaseTest {

    StartUpPage startUpPage;
    EmpRegistrationPage empRegistrationPage;


    @BeforeClass
    public void setUpBrowser() throws Exception {
        InitialiseBrowser();

    }

   @Test
    public void sitepassHomepage()
    {
        startUpPage = new StartUpPage();
        empRegistrationPage = new EmpRegistrationPage();
            try {
            accessPage(testproperties.getProperty("CBH_SitePassURL"), "Employee Online Registration - CBH Group - SitePass : Workforce Compliance Portal");
            clickButton(startUpPage.startButton);
            clickButton(startUpPage.tAndCcheckBox);
            clickButton(startUpPage.agreeButton);

            inputValue(empRegistrationPage.userName,"Employee_Rego_Test17");
            inputValue(empRegistrationPage.passWord, "Test123");
            inputValue(empRegistrationPage.confirmPassword, "Test123");
            selectByValue(empRegistrationPage.timeZone, "Australia/Canberra");
            selectByText(empRegistrationPage.businessName, "AGT");
            inputValue(empRegistrationPage.firstName, "John");
            inputValue(empRegistrationPage.lastName, "Smith");
            clickButton(empRegistrationPage.calendarButton);
            setDateInCalendar(empRegistrationPage.datesTable,empRegistrationPage.selectMonth,empRegistrationPage.selectYear, "30/06/1950");
            inputValue(empRegistrationPage.employeeID, "123456") ;
            inputValue(empRegistrationPage.eMail,"test@e3learning.com.au");
            inputValue(empRegistrationPage.phoneNumber, "08 12345678");
            inputValue(empRegistrationPage.mobileNumber, "0411111107");
            inputValue(empRegistrationPage.faxNumber, "08 12341234");
            selectByText(empRegistrationPage.country, "Australia");
            inputValue(empRegistrationPage.address, "40 Carrington Street");
            inputValue(empRegistrationPage.suburb, "Adelaide");
            selectByText(empRegistrationPage.state, "South Australia");
            inputValue(empRegistrationPage.postCode,"5000");
            selectByText(empRegistrationPage.jobType, "Cleaner");
            boolean checkElement = waitForElement(empRegistrationPage.servicedAreas, 10);
            if (checkElement == true) {
                clickButton(empRegistrationPage.servicedAreas);
                clickButton(empRegistrationPage.servicedAreaOptions);
                clickButton(empRegistrationPage.addServicedArea);
            }
            Thread.sleep(5000);
            ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
            clickButton(empRegistrationPage.nextButton);
            Thread.sleep(5000);
            String authURL = driver.getCurrentUrl();
            //System.out.println(authURL);
            String authNumber = empRegistrationPage.captureAuthCode();
            //System.out.println(authNumber);
            inputValue(empRegistrationPage.authNumber,authNumber);
            clickButton(empRegistrationPage.activateButton);
            Thread.sleep(5000);
            driver.switchTo().alert().accept();
            Thread.sleep(5000);
            String loginPage = driver.getTitle();
            System.out.println(loginPage);
                    } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @AfterClass
    public void closeBrowser(){
        driver.quit();
    }

    }



