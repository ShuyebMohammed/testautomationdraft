package com.e3learning.testAutomation.Sitepass.sitepassSelfRegistration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import sitepassSelfRegistration.BaseTest;


public class StartUpPage extends BaseTest {

    private WebDriver driver;


    //@FindBy(xpath = "//div[@id='content']/h2")
    //public WebElement welcomePage;

    @FindBy(xpath = "//a[contains(@href,'termsAndConditions')]")
    public WebElement startButton;

    //@FindBy
    //private WebElement tAndCPage;

    @FindBy(id = "conditions")
    public WebElement tAndCcheckBox;

    @FindBy(xpath = "//input[contains(@value,'Agree')]")
    public WebElement agreeButton;

    public StartUpPage() {
        this.driver = BaseTest.driver;
        PageFactory.initElements(driver, this);
    }


}

